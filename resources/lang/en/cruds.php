<?php

return [
    'userManagement' => [
        'title'          => 'User management',
        'title_singular' => 'User management',
    ],
    'permission'     => [
        'title'          => 'Permissions',
        'title_singular' => 'Permission',
        'fields'         => [
            'id'                => 'ID',
            'id_helper'         => ' ',
            'title'             => 'Title',
            'title_helper'      => ' ',
            'created_at'        => 'Created at',
            'created_at_helper' => ' ',
            'updated_at'        => 'Updated at',
            'updated_at_helper' => ' ',
            'deleted_at'        => 'Deleted at',
            'deleted_at_helper' => ' ',
        ],
    ],
    'role'           => [
        'title'          => 'Roles',
        'title_singular' => 'Role',
        'fields'         => [
            'id'                 => 'ID',
            'id_helper'          => ' ',
            'title'              => 'Title',
            'title_helper'       => ' ',
            'permissions'        => 'Permissions',
            'permissions_helper' => ' ',
            'created_at'         => 'Created at',
            'created_at_helper'  => ' ',
            'updated_at'         => 'Updated at',
            'updated_at_helper'  => ' ',
            'deleted_at'         => 'Deleted at',
            'deleted_at_helper'  => ' ',
        ],
    ],
    'user'           => [
        'title'          => 'Users',
        'title_singular' => 'User',
        'fields'         => [
            'id'                        => 'ID',
            'id_helper'                 => ' ',
            'name'                      => 'First Name',
            'name_helper'               => ' ',
            'email'                     => 'Email',
            'email_helper'              => ' ',
            'email_verified_at'         => 'Email verified at',
            'email_verified_at_helper'  => ' ',
            'password'                  => 'Password',
            'password_helper'           => ' ',
            'roles'                     => 'Roles',
            'roles_helper'              => ' ',
            'remember_token'            => 'Remember Token',
            'remember_token_helper'     => ' ',
            'created_at'                => 'Created at',
            'created_at_helper'         => ' ',
            'updated_at'                => 'Updated at',
            'updated_at_helper'         => ' ',
            'deleted_at'                => 'Deleted at',
            'deleted_at_helper'         => ' ',
            'approved'                  => 'Status',
            'approved_helper'           => ' ',
            'verified'                  => 'Verified',
            'verified_helper'           => ' ',
            'verified_at'               => 'Verified at',
            'verified_at_helper'        => ' ',
            'verification_token'        => 'Verification token',
            'verification_token_helper' => ' ',
            'program'                   => 'Which Program do you want to apply for?',
            'program_helper'            => ' ',
            'last_name'                 => 'Last Name',
            'last_name_helper'          => ' ',
            'full_name_en'              => 'Full Name in English as written in your ID/Passport',
            'full_name_en_helper'       => ' ',
            'full_name_ar'              => 'Full Name in Arabic as written in your ID/Passport (if applicable)',
            'full_name_ar_helper'       => ' ',
            'personal_photo'            => 'Personal Photo',
            'personal_photo_helper'     => ' ',
            'national'                  => 'Passport or National ID Number',
            'national_helper'           => ' ',
            'id_photo'                  => 'Photo of Passport or National ID',
            'id_photo_helper'           => ' ',
            'birth_date'                => 'Date of Birth',
            'birth_date_helper'         => ' ',
            'phone'                     => 'Phone Number (please add the country code)',
            'phone_helper'              => '(please add the country code)',
            'birth_country'             => 'Country of birth',
            'birth_country_helper'      => ' ',
            'country'                   => 'Country of residence',
            'country2'                  => 'Country',
            'nationality'               => 'Nationality',
            'country_helper'            => ' ',
            'state'                     => 'State of residence',
            'state_helper'              => ' ',
            'linkedin'                  => 'Link of LinkedIn Profile (if applicable)',
            'linkedin_helper'           => ' ',
            'undergraduate'             => 'Field of your undergraduate study',
            'undergraduate_helper'      => ' ',
            'degree'                    => 'Your latest educational degree',
            'degree_helper'             => ' ',
            'degree_photo'              => 'Degree Photo',
            'degree_photo_helper'       => ' ',
            'certificates'              => 'Photo(s) of Other Certificates (if applicable)',
            'certificates_helper'       => ' ',
            'cv'                        => 'Curriculum vitae (if applicable)',
            'cv_helper'                 => ' ',
            'personal_statement'        => 'Personal Statement',
            'personal_statement_helper' => ' ',
            'know_us'                   => 'How did you know about us?',
            'know_us_helper'            => ' ',
            'installment'            => 'has installment ',
        ],
    ],
    'student'           => [
        'title'          => 'Students',
        'title_singular' => 'Student',
        'binding'      => 'Pending',
        'Unchecked'    => 'New Applicant',
        'withdrawal'   => 'Withdrawal',
        'approved'     => 'Approved',
        'disapproved'  => 'Refused',
        'Unverified'   => 'Unverified',
    ],
    'visitor'           => [
        'title'          => 'Visitors',
        'title_singular' => 'Visitor',
        'binding'      => 'Pending',
        'Unchecked'    => 'New Applicant',
        'withdrawal'   => 'Withdrawal',
        'approved'     => 'Approved',
        'disapproved'  => 'Refused',
        'Unverified'   => 'Unverified',
    ],
    'userAlert'      => [
        'title'          => 'User Alerts',
        'title_singular' => 'User Alert',
        'fields'         => [
            'id'                => 'ID',
            'id_helper'         => ' ',
            'alert_text'        => 'Alert Text',
            'alert_text_helper' => ' ',
            'alert_link'        => 'Alert Link',
            'alert_link_helper' => ' ',
            'user'              => 'Users',
            'user_helper'       => ' ',
            'created_at'        => 'Created at',
            'created_at_helper' => ' ',
            'updated_at'        => 'Updated at',
            'updated_at_helper' => ' ',
        ],
    ],
    'faqManagement'  => [
        'title'          => 'FAQ Management',
        'title_singular' => 'FAQ Management',
    ],
    'faqCategory'    => [
        'title'          => 'Categories',
        'title_singular' => 'Category',
        'fields'         => [
            'id'                => 'ID',
            'id_helper'         => ' ',
            'category'          => 'Category',
            'category_helper'   => ' ',
            'created_at'        => 'Created at',
            'created_at_helper' => ' ',
            'updated_at'        => 'Updated At',
            'updated_at_helper' => ' ',
            'deleted_at'        => 'Deleted At',
            'deleted_at_helper' => ' ',
        ],
    ],
    'faqQuestion'    => [
        'title'          => 'Questions',
        'title_singular' => 'Question',
        'fields'         => [
            'id'                => 'ID',
            'id_helper'         => ' ',
            'category'          => 'Category',
            'category_helper'   => ' ',
            'question'          => 'Question',
            'question_helper'   => ' ',
            'answer'            => 'Answer',
            'answer_helper'     => ' ',
            'created_at'        => 'Created at',
            'created_at_helper' => ' ',
            'updated_at'        => 'Updated At',
            'updated_at_helper' => ' ',
            'deleted_at'        => 'Deleted At',
            'deleted_at_helper' => ' ',
        ],
    ],
    'payment'        => [
        'title'          => 'Payment',
        'lectures'          => 'Lectures Payments',
        'programs'          => 'Programs Payments',
        'title_singular' => 'Payment',
        'fields'         => [
            'id'                 => 'ID',
            'id_helper'          => ' ',
            'user'               => 'User',
            'user_helper'        => ' ',
            'transaction'        => 'Transaction ID',
            'transaction_helper' => ' ',
            'amount'             => 'Amount',
            'amount_helper'      => ' ',
            'status'             => 'Status',
            'status_helper'      => ' ',
            'created_at'         => 'Created at',
            'created_at_helper'  => ' ',
            'updated_at'         => 'Updated at',
            'updated_at_helper'  => ' ',
            'deleted_at'         => 'Deleted at',
            'deleted_at_helper'  => ' ',
            'created_by'         => 'Created By',
            'created_by_helper'  => ' ',
        ],
    ],
    'payNow'         => [
        'title'          => 'Pay Now',
        'title_singular' => 'Pay Now',
    ],
    'lecture'        => [
        'title'          => 'Lectures',
        'title_singular' => 'Lecture',
        'fields'         => [
            'id'                    => 'ID',
            'id_helper'             => ' ',
            'name'                  => 'Name',
            'name_helper'           => 'Please Enter the Lecture name that will appear while applying',
            'date'                  => 'Lecture Date',
            'date_helper'           => ' ',
            'time'                  => 'Lecture Time',
            'time_helper'           => ' ',
            'instructor'            => 'Instructor Name',
            'instructor_helper'     => ' ',
            'price_forign'          => 'Price for Forign',
            'price_forign_helper'   => 'this is the price that all forign will pay',
            'price_egyption'        => 'Price for Egyption',
            'price_egyption_helper' => 'this is the price that will egyption pay',
            'description'           => 'Description',
            'description_helper'    => 'this feild is optional if we want to give more details',
            'created_at'            => 'Created at',
            'created_at_helper'     => ' ',
            'updated_at'            => 'Updated at',
            'updated_at_helper'     => ' ',
            'deleted_at'            => 'Deleted at',
            'deleted_at_helper'     => ' ',
        ],
    ],
    'system_email'        => [
        'title'          => 'System Emails',
        'title_singular' => 'System Email',
        'fields'         => [
            'id'                    => 'ID',
            'id_helper'             => ' ',
            'name'                  => 'Name',
            'slug'                  => 'Slug',
            'subject'               => 'Subject',
            'subject_helper'           => 'The email subject',
            'date'                  => 'System Email Date',
            'date_helper'           => ' ',
            'time'                  => 'System Email Time',
            'time_helper'           => ' ',
            'instructor'            => 'Instructor Name',
            'instructor_helper'     => ' ',
            'price_forign'          => 'Price for Forign',
            'price_forign_helper'   => 'this is the price that all forign will pay',
            'price_egyption'        => 'Price for Egyption',
            'price_egyption_helper' => 'this is the price that will egyption pay',
            'description'           => 'Description',
            'message'           => 'Message',
            'message_helper'    => 'The content of the email',
            'created_at'            => 'Created at',
            'created_at_helper'     => ' ',
            'updated_at'            => 'Updated at',
            'updated_at_helper'     => ' ',
            'deleted_at'            => 'Deleted at',
            'deleted_at_helper'     => ' ',
        ],
    ],'user_log'        => [
        'title'          => 'User Logs',
        'title_singular' => 'User Log',
        'fields'         => [
            'id'                    => 'ID',
            'id_helper'             => ' ',
            'name'                  => 'Name',
            'slug'                  => 'Slug',
            'subject'               => 'Subject',
            'subject_helper'           => 'The email subject',
            'date'                  => 'System Email Date',
            'date_helper'           => ' ',
            'time'                  => 'System Email Time',
            'time_helper'           => ' ',
            'instructor'            => 'Instructor Name',
            'instructor_helper'     => ' ',
            'price_forign'          => 'Price for Forign',
            'price_forign_helper'   => 'this is the price that all forign will pay',
            'price_egyption'        => 'Price for Egyption',
            'price_egyption_helper' => 'this is the price that will egyption pay',
            'description'           => 'Description',
            'message'           => 'Message',
            'message_helper'    => 'The content of the email',
            'created_at'            => 'Created at',
            'created_at_helper'     => ' ',
            'updated_at'            => 'Updated at',
            'updated_at_helper'     => ' ',
            'deleted_at'            => 'Deleted at',
            'deleted_at_helper'     => ' ',

            'user'     => 'User',
        ],
    ],


];
