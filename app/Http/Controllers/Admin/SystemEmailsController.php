<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroySystemEmailRequest;
use App\Http\Requests\StoreSystemEmailRequest;
use App\Http\Requests\UpdateSystemEmailRequest;
use App\Models\SystemEmail;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Symfony\Component\HttpFoundation\Response;

use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Notification;

use App\Notifications\VisitorWelcomeEmail;

class SystemEmailsController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('system_email_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $system_emails = SystemEmail::all();

        return view('admin.systemEmails.index', compact('system_emails'));
    }
    public function testmail(){
        $users = User::find(Auth::user()->id);
        Notification::send($users, new VisitorWelcomeEmail($users));
        die("----");
    }

    public function create()
    {
        abort_if(Gate::denies('system_email_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.systemEmails.create');
    }

    public function store(StoreSystemEmailRequest $request)
    {
        $system_email = SystemEmail::create($request->all());

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $system_email->id]);
        }

        return redirect()->route('admin.system-emails.index');
    }

    public function edit(SystemEmail $system_email)
    {
        abort_if(Gate::denies('system_email_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.systemEmails.edit', compact('system_email'));
    }

    public function update(UpdateSystemEmailRequest $request, SystemEmail $system_email)
    {
        $system_email->update($request->all());

        return redirect()->route('admin.system-emails.index');
    }

    public function show(SystemEmail $system_email)
    {
        abort_if(Gate::denies('system_email_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.systemEmails.show', compact('system_email'));
    }

    public function destroy(SystemEmail $system_email)
    {
        abort_if(Gate::denies('system_email_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $system_email->delete();

        return back();
    }

    public function massDestroy(MassDestroySystemEmailRequest $request)
    {
        SystemEmail::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('system_email_create') && Gate::denies('system_email_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new SystemEmail();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}
