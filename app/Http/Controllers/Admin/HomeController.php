<?php

namespace App\Http\Controllers\Admin;
use App\Models\Lecture;
class HomeController
{
    public function index()
    {
    	$lectures = Lecture::where('date','>',date("Y/m/d"))->orderBy('date', 'ASC')->get();
    	$all_lectures = $lectures->toArray();
        return view('home', compact('all_lectures'));
    }
}
