<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\CsvImportTrait;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyUserRequest;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\Role;
use App\Models\User;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Symfony\Component\HttpFoundation\Response;

class UnverifiedController extends Controller
{
    use MediaUploadingTrait, CsvImportTrait;

    public function index()
    {
        abort_if(Gate::denies('Unverified_student_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        if(!Gate::denies('Egyptology_Student')){
            $users = User::where('program','like','%Egyptolog%')->where('status','like','Unverified')->get();
        }else if(!Gate::denies('BioHealth_Student')){
            $users = User::where('program','like','%Bio-Health%')->where('status','like','Unverified')->get();
        }else{
            $users = User::where('status','like','Unverified')->get();
        }
        //$users = User::where('status','like','Unverified')->get();

        $roles = Role::get();

        return view('admin.users.index', compact('users', 'roles'));
    }
    public function visitors()
    {

        abort_if(Gate::denies('binding_student_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $users = User::where('status','like','Unverified')->whereHas('roles', function($q){
            $q->where('id', '=', 7);
        })->get();

        $roles = Role::get();

        return view('admin.users.index', compact('users', 'roles'));
    }


}
